<?php

declare (strict_types = 1);

namespace Chenm\Imi\websafe;

use Imi\Log\Log;
use Imi\Main\BaseMain;

class Main extends BaseMain {
    public function __init(): void {
        Log::info('沉梦web防火墙已启动~');
    }
}
