# imi-websafe

#### 介绍
基于IMI的Web防火墙

#### 软件架构
基于IMI的Web防火墙，纯php写的，安全日志自动记录并自动删除过期日志


#### 安装教程

使用如下composer命令即可一键安装使用

```
composer require chenm/imi-websafe
```

